import React, { FC } from "react";
import { Grid, CircularProgress } from '@mui/material';

const Loader: FC = () => {
    return (
        <Grid container justifyContent="center" >
            <CircularProgress />
        </Grid>
    );
};

export default Loader;
