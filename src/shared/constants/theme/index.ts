import { createTheme } from "@mui/material/styles";
import { common } from "@mui/material/colors";

const theme = createTheme({
  breakpoints: {
    values: {
      xs: 0,
      sm: 600,
      md: 960,
      lg: 1440,
      xl: 1920,
    },
  },
  spacing: 1,
  palette: {
    common: {
      black: common.black,
      white: common.white,
    },
    primary: {
      main: "#130c0e",
      contrastText: "#dddce5",
    },
    secondary: {
      main: "#3f3639",
      contrastText: "#dddce5",
    },
    error: {
      main: "#F44336",
    },
    grey: {
      100: "#F6F3F2",
      300: "#9e9e9e",
      500: "#666666",
      600: "#495057",
    },
    text: {
      primary: '#ddd',
      secondary: '#aaa',
      disabled: '#edec51',
    },
    background: {
      default: "#505050",
      paper: common.white,
    },
  },
  typography: {
    h1: {
      fontWeight: 400,
      fontSize: "42px",
    },
    h5: {
      fontWeight: 400,
      fontSize: "24px",
      lineHeight: 1,
    },
    h6: {
      fontWeight: 500,
      fontSize: "20px",
      lineHeight: "24px",
    },
    subtitle1: {
      fontWeight: 500,
      fontSize: "24px",
      lineHeight: 1,
    },
    subtitle2: {
      fontWeight: 500,
      fontSize: "14px",
      lineHeight: "24px",
    },
    body1: {
      fontWeight: 400,
      fontSize: "16px",
      lineHeight: "24px",
    },
    body2: {
      fontWeight: 400,
      fontSize: "14px",
      lineHeight: "24px",
    },
    button: {
      fontWeight: 500,
      fontSize: "14px",
      lineHeight: "24px",
    },
    caption: {
      fontWeight: 400,
      fontSize: "12px",
      lineHeight: "14px",
    },
    overline: {
      fontWeight: 400,
      fontSize: "10px",
      lineHeight: "24px",
    },
  },
});

export default theme;
