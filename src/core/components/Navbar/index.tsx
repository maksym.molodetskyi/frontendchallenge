import React, { useState, MouseEvent } from 'react';
import { AppBar, Box, Toolbar, IconButton, Typography, Menu, Container, MenuItem, Link } from "@mui/material";
import { Link as RouterLink } from "react-router-dom";
import MenuIcon from '@mui/icons-material/Menu';

const navbarTitle = "Star Wars";
const pages = [
    {
        name: "Movies",
        to: "/movies",
    },
    {
        name: "Characters",
        to: "/characters",
    },
];

const Navbar = () => {
    const [anchorElNav, setAnchorElNav] = useState<null | HTMLElement>(null);

    const handleOpenNavMenu = (event: MouseEvent<HTMLElement>) => {
        setAnchorElNav(event.currentTarget);
    };

    const handleCloseNavMenu = () => {
        setAnchorElNav(null);
    };

    return (
        <AppBar position="static">
            <Container maxWidth="xl">
                <Toolbar disableGutters>
                    <Box mx={10} sx={{ mr: 10, display: { xs: 'none', md: 'flex' } }}>
                        <Typography
                            variant="h6"
                            noWrap
                            component="div"
                        >
                            {navbarTitle}
                        </Typography>
                    </Box>
                    

                    <Box sx={{ flexGrow: 1, display: { xs: 'flex', md: 'none' } }}>
                        <IconButton
                            size="large"
                            aria-label="account of current user"
                            aria-controls="menu-appbar"
                            aria-haspopup="true"
                            onClick={handleOpenNavMenu}
                            color="inherit"
                        >
                            <MenuIcon />
                        </IconButton>
                        <Menu
                            id="menu-appbar"
                            anchorEl={anchorElNav}
                            anchorOrigin={{
                                vertical: 'bottom',
                                horizontal: 'left',
                            }}
                            keepMounted
                            transformOrigin={{
                                vertical: 'top',
                                horizontal: 'left',
                            }}
                            open={Boolean(anchorElNav)}
                            onClose={handleCloseNavMenu}
                            sx={{
                                display: { xs: 'block', md: 'none' },
                            }}
                        >
                            {pages.map((item) => (
                                <MenuItem key={item.name} onClick={handleCloseNavMenu}>
                                    <Link
                                        component={RouterLink}
                                        to={item.to}
                                        style={{ textDecoration: "none", marginRight: "10px" }}
                                    >
                                        <Typography color="secondary" textAlign="center">{item.name}</Typography>
                                    </Link>
                                </MenuItem>
                            ))}
                        </Menu>

                    </Box>
                    <Typography
                        variant="h6"
                        noWrap
                        component="div"
                        sx={{ flexGrow: 1, display: { xs: 'flex', md: 'none' } }}
                    >
                        {navbarTitle}
                    </Typography>
                    <Box sx={{ flexGrow: 1, display: { xs: 'none', md: 'flex' } }}>
                        {pages.map((item) => ( 
                            <Link
                            key={item.name}
                                component={RouterLink}
                                to={item.to}
                                style={{ textDecoration: "none", marginLeft: "30px" }}
                            >
                                <Typography color="secondary" textAlign="center">{item.name}</Typography>
                            </Link>
                        ))}
                    </Box>
                </Toolbar>
            </Container>
        </AppBar>
    );
};
export default Navbar;