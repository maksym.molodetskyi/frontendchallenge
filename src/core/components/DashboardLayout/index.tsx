import React, { FC, ReactElement } from "react";
import { Container, Box } from "@mui/material";
import { Outlet } from "react-router-dom";

import Navbar from "core/components/Navbar";

import useStyles from "./styles";

const DashboardLayout: FC = (): ReactElement => {
  const classes = useStyles();

  return (
    <Container className={classes.wrapper}>
      <Navbar />
      <Box py={20} px={20}>
        <Container className={classes.contentWrapper}>
          <Outlet />
        </Container>
      </Box>

    </Container>
  );
};

export default DashboardLayout;
