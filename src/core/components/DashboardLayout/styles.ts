import { makeStyles } from '@mui/styles';
import { createTheme, Theme } from "@mui/material/styles";

createTheme();

const useStyles = makeStyles((theme: Theme) => ({
  wrapper: {
    maxWidth: "100%",
    display: "flex",
    backgroundColor: '#282727',
    padding: theme.spacing(0),
  },
  contentWrapper: {
    maxWidth: "99vw",
  },
})
);

export default useStyles;
