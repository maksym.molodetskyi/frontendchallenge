import { createStore, createHook, Action } from "react-sweet-state";
import { IFilm } from "swapi-ts";

interface IFilmData {
  films: IFilm[],
  loading: boolean,
}

type Actions = typeof actions;

const initialState: IFilmData = {
  films: [],
  loading: false,
};

const actions = {
  setLoading:
    (status: boolean): Action<IFilmData> =>
      ({ setState, getState }) => {
        setState({
          ...getState(),
          loading: status,
        });
      },
  fetchFilms:
    (): Action<IFilmData> =>
      async ({ setState, dispatch }) => {
        dispatch(actions.setLoading(true));
        const response = await fetch('https://swapi.dev/api/films/');
        const data = await response.json();

        setState({
          films: data.results,
          loading: false,
        });
      },
};

const Store = createStore<IFilmData, Actions>({
  initialState: initialState,
  actions,
});

export const useFilms = createHook(Store);