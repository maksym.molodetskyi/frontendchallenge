import { createStore, createHook, Action } from "react-sweet-state";
import { IPeople } from "swapi-ts";

interface IPeopleData {
  peopleLinks?: string[],
  people?: IPeople[],
  loading?: boolean,
}

type Actions = typeof actions;

const initialState: IPeopleData = {
  peopleLinks: [],
  people: [],
  loading: false,
};

const actions = {
  setLoading:
    (status: boolean): Action<IPeopleData> =>
      ({ setState, getState }) => {
        setState({
          ...getState(),
          loading: status,
        });
      },
  addUniquePeopleLinks:
    (array: string[]): Action<IPeopleData> =>
      ({ setState, getState, dispatch }) => {
        const links = getState().peopleLinks || [];
        const uniqePeople = array.filter((item: string) => !links?.includes(item));

        setState({
          ...getState(),
          peopleLinks: [...links, ...uniqePeople],
        });
        dispatch(actions.fetchPeople(uniqePeople));
      },
  fetchPeople:
    (array: string[]): Action<IPeopleData> =>
      async ({ setState, getState, dispatch }) => {
        dispatch(actions.setLoading(true));
        const peopleData = await Promise.all(array.map(async (url: string) => {
          const resp = await fetch(url);
          return resp.json();
        }));

        setState({
          ...getState(),
          people: [...(getState().people || []), ...peopleData],
          loading: false,
        });
      },
};

const Store = createStore<IPeopleData, Actions>({
  initialState: initialState,
  actions,
});

export const usePeople = createHook(Store);
