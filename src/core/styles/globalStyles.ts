import { withStyles } from "@mui/styles";


// ----------------------------------------------------------------------

const GlobalStyles = withStyles((theme) => ({
  "@global": {
    html: {
      width: "100%",
      minHeight: "100vh",
      "-ms-text-size-adjust": "100%",
      "-webkit-overflow-scrolling": "touch",
      overflowY: "scroll",
    },
    body: {
      width: "100%",
      height: "100%",
    },
    "#root": {
      height: "100%",
      width: "100%",

      "& > .MuiContainer-root": {
        height: "100%",
      }
    },
    input: {
      "&[type=number]": {
        MozAppearance: "textfield",
        "&::-webkit-outer-spin-button": { margin: 0, WebkitAppearance: "none" },
        "&::-webkit-inner-spin-button": { margin: 0, WebkitAppearance: "none" },
      },
    },
    textarea: {
      "&::-webkit-input-placeholder": { color: theme.palette.text.disabled },
      "&::-moz-placeholder": { opacity: 1, color: theme.palette.text.disabled },
      "&:-ms-input-placeholder": { color: theme.palette.text.disabled },
      "&::placeholder": { color: theme.palette.text.disabled },
    },
    a: { color: theme.palette.primary.main },
    img: { display: "block", maxWidth: "100%" },
    ".card-item": {
      [theme.breakpoints.down("sm")]: {
        width: "100%",
        textAlign: "center",
      },
    }
  },
}))(() => null);

export default GlobalStyles;
