import React, { FC } from "react";
import { Card, CardActionArea, CardContent, Typography, Grid } from '@mui/material';
import { useNavigate } from "react-router-dom";

import { IFilm } from "swapi-ts";
import { MOVIES } from "routing/paths";
import { usePeople } from "core/context/People";

interface IMoviesGridProps {
  data: IFilm[];
}
const MoviesGrid: FC<IMoviesGridProps> = ({
  data = [],
}) => {
  const navigate = useNavigate();
  const [, { addUniquePeopleLinks }] = usePeople();

  const handleNavigation = (film: IFilm, index: number) => {
    addUniquePeopleLinks(film.characters as string[]);
    navigate(`${MOVIES}/${index}`);
  }

  return (
    <Grid container justifyContent="center">
      {data.map((film: IFilm, index) => (
        <Card
          className="card-item"
          sx={{ maxWidth: 345, margin: '10px' }}
          key={film.title}
          onClick={() => handleNavigation(film, index)}
        >
          <CardActionArea>
            <CardContent>
              <Typography variant="h5" component="div">
                {film.title}
              </Typography>
            </CardContent>
          </CardActionArea>
        </Card>
      ))}
    </Grid>
  );
};

export default MoviesGrid;