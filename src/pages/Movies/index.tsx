import React, { FC, useEffect } from "react";

import Loader from "shared/components/Loader";
import MoviesGrid from "./components/MoviesGrid";
import { useFilms } from "core/context/Films";

export const Movies: FC = () => {
  const [filmsState, { fetchFilms }] = useFilms();

  useEffect(() => {
    if (!filmsState.films.length) {
      fetchFilms();
    }
  }, [filmsState.films.length, fetchFilms])
  return (
    <>
      {filmsState.loading ? (
        <Loader />
      ) : (
        <>
          <h2>Movies:</h2>
          <MoviesGrid data={filmsState.films} />
        </>
      )
      }
    </>
  );
};

