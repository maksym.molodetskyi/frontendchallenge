import React, { FC } from "react";

import CharactersGrid from "./components/CharactersGrid";
import { usePeople } from "core/context/People";
import Loader from "shared/components/Loader";

export const Characters: FC = () => {
  const [peopleState] = usePeople();

  return (
    <>
      {peopleState.loading ? (
        <Loader />
      ) : (
        <>
          <h2>Characters:</h2>
          <CharactersGrid data={peopleState.people || []} />
        </>
      )
      }
    </>
  );
};