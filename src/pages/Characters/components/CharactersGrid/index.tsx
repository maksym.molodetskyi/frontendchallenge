import React, { FC } from "react";
import { Card, CardActionArea, CardContent, Typography, Grid } from '@mui/material';
import { useNavigate } from "react-router-dom";

import { IPeople } from "swapi-ts";
import { CHARACTERS } from "routing/paths";

interface ICharactersGridProps {
  data: IPeople[];
}

const CharactersGrid: FC<ICharactersGridProps> = ({
  data = [],
}) => {
  const navigate = useNavigate();

  const handleNavigation = (character: IPeople, index: number) => {
    navigate(`${CHARACTERS}/${index}`);
  }

  return (
    <Grid container justifyContent="center">
      {data.map((character: IPeople, index) => (
        <Card
          className="card-item"
          sx={{ maxWidth: 345, margin: '10px' }}
          key={character.name}
          onClick={() => handleNavigation(character, index)}
        >
          <CardActionArea>
            <CardContent>
              <Typography variant="h5" component="div">
                {character.name}
              </Typography>
            </CardContent>
          </CardActionArea>
        </Card>
      ))}
    </Grid>
  );
};

export default CharactersGrid;