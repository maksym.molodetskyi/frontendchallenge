import React, { FC, useEffect, useState } from "react";
import { Card, CardContent, Typography, Grid, Box } from '@mui/material';
import Loader from "shared/components/Loader";

import { useParams } from "react-router-dom";
import { usePeople } from "core/context/People";

import { IPeople } from "swapi-ts";

export const SingleCharacter: FC = () => {
  const { id } = useParams();
  const [peopleState] = usePeople();
  const [characterData, setCharacterData] = useState<IPeople | null>(null);
  const [loading, setLoading] = useState<boolean>(true);

  const fieldsArray = [
    {
      title: "Name",
      value: characterData?.name,
    },
    {
      title: "Gender",
      value: characterData?.gender
    },
    {
      title: "Skin color",
      value: characterData?.skin_color
    },
    {
      title: "Hair color",
      value: characterData?.hair_color
    },
    {
      title: "Eye color",
      value: characterData?.eye_color
    },
  ]

  useEffect(() => {
    if (peopleState?.people?.length) {
      if (id !== undefined && peopleState.people.length >= +id) {
        setCharacterData(peopleState.people[+id]);
      }
    } else {
      setCharacterData(null);
    }
    setLoading(false);
  }, [peopleState.people, id])
  return (
    <>
      {loading ? (
        <Loader />
      ) : (
        <>
          <h2>Single Character:</h2>
          {characterData && (<Card sx={{ minWidth: 275, background: "#505050", maxWidth: "800px", mx: "auto" }}>
            <CardContent>
              {fieldsArray.map((item) => (
                <Box key={item.title} mb={20}>
                  <Grid container justifyContent="space-between" wrap="nowrap">
                    <Box mr={10}>
                      <Typography variant="h6">{`${item.title}:`}</Typography>
                    </Box>
                    <Box>
                      <Typography align="right" variant="h5">
                        {item.value}
                      </Typography>
                    </Box>
                  </Grid>
                </Box>
              ))}
            </CardContent>
          </Card>
          )}
        </>
      )}
    </>
  );
};

