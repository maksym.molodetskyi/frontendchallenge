import React, { FC, useEffect, useState } from "react";
import { Card, CardContent, Typography, Grid, Box } from '@mui/material';
import Loader from "shared/components/Loader";
import { useParams } from "react-router-dom";
import { useFilms } from "core/context/Films";

import { IFilm } from "swapi-ts";

export const SingleMovie: FC = () => {
  const { id } = useParams();
  const [filmsState] = useFilms();
  const [filmData, setFilmData] = useState<IFilm | null>(null);
  const [loading, setLoading] = useState<boolean>(true);

  const fieldsArray = [
    {
      title: "Title",
      value: filmData?.title,
    },
    {
      title: "Director",
      value: filmData?.director
    },
    {
      title: "Producer",
      value: filmData?.producer
    },
    {
      title: "Description",
      value: filmData?.opening_crawl
    },
  ]

  useEffect(() => {
    async function fetchFilm() {
      const response = await fetch(`https://swapi.dev/api/films/${Number(id) + 1}`);
      const result = await response.json();

      setFilmData(result)
      setLoading(false);
    }

    if (filmsState.films.length) {
      if (id !== undefined && filmsState.films.length >= +id) {
        setFilmData(filmsState.films[+id]);
        setLoading(false);
      }
    } else {
      fetchFilm()
    }
  }, [filmsState.films, id])
  return (
    <>
      {loading ? (
        <Loader />
      ) : (
        <>
          <h2>Single Movie:</h2>
          <Card sx={{ minWidth: 275, background: "#505050" }}>
            <CardContent>
              {fieldsArray.map((item) => (
                <Box key={item.title} mb={20}>
                  <Grid container justifyContent="space-between" wrap="nowrap">
                    <Box mr={10}>
                      <Typography variant="h6">{`${item.title}:`}</Typography>
                    </Box>
                    <Box>
                      <Typography align="right" variant="h5">
                        {item.value}
                      </Typography>
                    </Box>
                  </Grid>
                </Box>
              ))}
            </CardContent>
          </Card>
        </>
      )
      }
    </>
  );
};

