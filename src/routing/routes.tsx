import { Navigate, useRoutes } from "react-router-dom";
import { CHARACTERS, MOVIES } from "./paths";

// layouts
import DashboardLayout from "core/components/DashboardLayout";

//pages
import { Movies } from "pages/Movies";
import { SingleMovie } from "pages/SingleMovie";
import { Characters } from "pages/Characters";
import { SingleCharacter } from "pages/SingleCharacter";

export default function RouterApp() {
  return useRoutes([
    {
      path: "/",
      element: <DashboardLayout />,
      children: [
        {
          path: "/",
          element: <Navigate to={MOVIES} />,
        },
        {
          path: MOVIES,
          element: <Movies />,
        },
        {
          path: `${MOVIES}/:id`,
          element: <SingleMovie />
        },
        {
          path: CHARACTERS,
          element: <Characters />
        },
        {
          path: `${CHARACTERS}/:id`,
          element: <SingleCharacter />
        },
      ],
    },
  ]);
}
