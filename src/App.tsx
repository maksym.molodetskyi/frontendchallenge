import * as React from "react";

import RouterApp from "routing/routes";
import { BrowserRouter as Router } from "react-router-dom";

export default function App() {
  return (
    <Router>
      <RouterApp />
    </Router>
  );
}
